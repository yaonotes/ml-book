import numpy as np

X = np.array([2, 3, 4, 14, 12, 13, 6, 9, 6])
Y = np.array([3, 1, 2,  5,  4,  7, 5, 4, 4])
centers = [np.array([3,1]), np.array([9,4])]
MAX_ITER = 10

def cost(X,Y,clusters):
    pass

def distance(x, y):
    return np.linalg.norm(x-y, 2)

def run_cluster(X,Y, centers):
    clusters = {}
    for i in range(len(centers)):
        clusters[i] = []
    for index, each in enumerate(X):
        coord = np.array([each, Y[index]])
        distances = [distance(coord, center) for center in centers]
        classification = distances.index(min(distances))
        clusters[classification].append(coord)
    return clusters

clusters = run_cluster(X,Y,centers)
print(clusters)