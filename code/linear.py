import numpy as np
import matplotlib.pyplot as plt

def calculate_weight(X,Y):
    A = np.vstack([X, np.ones(len(X))]).T
    weight, residual = np.linalg.lstsq(A, Y, rcond=None)[0]
    return weight, residual

def high_order_weight(X_mat, Y):
    xinv = np.linalg.inv(np.matmul(X_mat.T,X_mat))
    xweight = np.matmul(xinv,(X_mat.T))
    return  xweight @ Y

def plot_mc(X,Y, w,r):
    _ = plt.plot(X, Y, 'o', label='Original data', markersize=10)
    _ = plt.plot(X, w*X + r, 'r', label='Fitted line')
    _ = plt.legend()
    plt.show()

X=np.array([2,3,4,5,6,7])
Y=np.array([5,6,5,9,7,10])

weight,residual=calculate_weight(X,Y)
# plot_mc(X,Y,weight, residual)

X_m=np.array([[1,2,4],[1,3,9],[1,4,16],[1,5,25],[1,6,36],[1,7,49]])
new_weight = high_order_weight(X_m, Y)
print(new_weight)