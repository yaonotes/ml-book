import sympy
from sympy.core.symbol import symbols

def log_exp(x):
    return sympy.ln(sympy.exp(x)+sympy.exp(-x))

x=symbols('x')
one_order_diff = sympy.diff(log_exp(x),x)
two_order_diff = sympy.diff(log_exp(x),x, 2)

start = 1
estimation = 0
iterations = 4
step_size = 1
new_x = start
for i in range(iterations):
    new_x = new_x - step_size * one_order_diff.evalf(subs={x: new_x}) * (1/two_order_diff.evalf(subs={x: new_x}))
    print("new x: {}".format(new_x))
    print("f(x): {}".format(log_exp(new_x)))
    print("f(x)-f(x^\star):{}".format( log_exp(new_x)-log_exp(estimation) ))