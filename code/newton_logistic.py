import numpy as np

def sigmoid(x):
    return 1/(1+np.exp(-x))

def dsigmoid(x):
    return sigmoid(x)/(1-sigmoid(x))

def mu(w,x_i):
    return sigmoid(np.dot(w, x_i))

def calc_hessian_diagonal(X,w):
    diags = []
    for each in X:
        mu_value = mu(w, each)*(1-mu(w,each))
        diags.append(mu_value)
    res = X.T @ np.diag(np.array(diags)) @ X
    return res

def calc_gradient(X,Y, w):
    mu_s = []
    for each in X:
        mu_s.append(mu(w, each))
    mu_s = np.array(mu_s).reshape(Y.shape)
    res = X.T@(mu_s-Y)
    return X.T@(mu_s-Y)

X = np.array([[1,8,2],[1,5,5],[1,7,7],[1,9,8],[1,3,8],[1,4,5]])
Y = np.array([[1],[1],[1],[0],[0],[0]])
start_weight = np.array([0,0,0])
iterations = 10
for i in range(iterations):
    print(start_weight)
    hess = calc_hessian_diagonal(X, start_weight)
    grad = calc_gradient(X,Y, start_weight)
    start_weight = start_weight - (np.linalg.inv(hess) @ grad).T
    start_weight = start_weight.reshape(-1)