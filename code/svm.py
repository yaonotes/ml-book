import numpy as np
import matplotlib.pyplot as plt
# =================================================================
# Linear Separability Check
# =================================================================
def show_x_y(X,Y):

    _ = plt.plot(X, Y, 'o', label='Original data', markersize=10)
    plt.show()
X = np.array([-3,-2,-1,0,1,3])
Y = np.array([1,1,-1,-1,-1,1])

show_x_y(X,Y)