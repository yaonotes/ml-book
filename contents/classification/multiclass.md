---
title: Multiclass Classification
---

# Multiclass Classification

**<u>Classifiers</u>**

In practice, there are two common approaches

* One-vs-One:
  * Train $c \choose 2$ different classifiers for all pairs of classes, i.e. $\frac{c(c-1)}{2}$ classifiers.
  * Each training procedure only uses on average $\frac{2}{k}$ of the training data.
  * For new  input, choose the most common classification for the classifiers.
* One-vs-Rest
  * Train $C$ different classifiers, one class vs. the rest $C-1$.
  * It is typical for classifiers that yield class membership probability or scores.
  * For new input $x$, pick the class with the largest probability or score.

### Measuring Performance

In Regression, we us the same loss function to test data as for training. In classification, we use the number of misclassified data points as classification error.

**<u>Confusion Matrix</u>**

For binary classification, we can use the confusion matrix.

| Prediction | Actual Labels  | Actual Labels  |
| ---------- | -------------- | -------------- |
|            | Yes            | No             |
| Yes        | True Positive  | False Positive |
| No         | False Negative | True Negative  |

* True positive rate, sensitivity, Recall: $\text{TPR}=\frac{TP}{TP+FN}$. TPR is the ratio of true positives to actual positives.
* False Positive Rate, Fall-out: $\text{FPR}=\frac{FP}{FP+TN}$. FPR is the ratio of false positives to actual negatives.
* True negative rate, Specificity: $\text{TNR}=1-\text{FPR}$.
* Precision: $\text{P}=\frac{\text{TP}}{\text{TP}+\text{FP}}$. It is the ratio of true positives to predicted positives.
* Accuracy: $\text{Acc}=\frac{\text{TP+TN}}{\text{P}+\text{N}}$

The confusion matrix can be easily extended into multiclass classification.

**<u>Receiver Operating Characteristic (ROC)</u>**

ROC space is defined by $\frac{\text{TPR}}{\text{FPR}}$

**<u>Areas under the ROC Curve (AUC)</u>**

AUC makes it easy to compare ROC curves for diferent classsifers.

**<u>Precision-Recall Curves</u>**

Beyond ROC curves, replace $\text{FPR}$ with $\text{Precision}$.

