---
title: Clustering
---

# Clustering

Often the data can be grouped together into subsets that are coherent. However, this grouping maybe subjective and it is hard to define a general framework.

Two types of Clustering algorithms

* Feature-based: Points are represented as vectors in $\mathbb{R}^d$.
* Similarity-based: only know pairwise similarities.

Two types of clustering methods

* Flat: Partition the data into $k$ clusters.
* Hierarchical: Organise data as clusters, clusters of clusters, and so on.



## $K$-Means

**<u>Goal</u>**: Partition the data into subsets $C_1,\cdots,C_k$ where $k$ is fixed.

The quality of partition is defined by 
$$
W(C)=\frac{1}{2}\sum_{j=1}^k\frac{1}{|C_j|}\sum_{i,i'\in C_j}d(x_i,x_i')
$$
If we use $d(x,x')=||x-x'||_2^2$, then
$$
W(C)=\sum_{j=1}^k\sum_{i\in C_j}||x_i-\mu_j||_2^2
$$
where $\mu_j=\frac{1}{|C_j|}\sum_{i\in C_j}x_i$. Hence, the objective is minimise the sum of squares of distances to the mean within each cluster.

However, this problem is NP-hard even for $k=2$.

* ***Assignment***. If we fix means $\mu_1,\cdots,\mu_k$, finding a partition that minimises $W$ is easy.
* ***Update***. If we fix the clusters $C_1,\cdots,C_k$, minimising $W$ w.r.t $\mu_j$ is easy.

So, an alternating minimisation is iteratively run these assignment and update steps.

The $k$-means clustering algorithm can be illustrated below:

* Initialise means $\mu_1,\cdots,\mu_k$ randomly.

* Repeat until convergence.

  * Construct clusters $C_1,\cdots,C_k$ by assigning the data to clusters represented by their means.
    $$
    C_j=\{i|j=\text{argmin}_{j'}||x_i-\mu_{j'}||_2^2\}
    $$
    

  * Update means using the current cluster assignments.
    $$
    \mu_j=\frac{1}{|C_j|}\sum_{i\in C_j}x_i
    $$

* 

**<u>Convergence of $k$-means</u>**

The $k$-means algorithm always converge, because the objective function $W$ decreases every time a new partition is used. There are only finitely many partitions. The convergence is likely to a local minimum and we should run multiple times with random initialisation.

**<u>Choosing the number of clusters $k$</u>**

It is not easy, we usually need to plot $W$ against $k$ and identify an elbow. Larger $k$ lowers the 

## Beyond $K$-means

**<u>$K$-medoids</u>**

The $K$-medoids use actual data points as cluster means and not Euclidean averages. It can use any distance between points beyond the squared Euclidean distance. In particular, we can use any other $\ell_p$ norm.

**<u>$K$-Center</u>**

The objective is to maximum overall dissimilarities between data and anchor. For $k$-means, it sum of all the dissimilarities in each cluster.

## Transforming the Input

**<u>Dissimilarity</u>**

The weighted dissimilarity between real valued attributes is defined as 
$$
d(x,x')=f(\sum_{i=1}^Dw_id_i(x_i,x_i'))
$$
The simplest setting is $w_i=1$, $d_i(x_i,x_i')=(x_i-x_i')^2$ and $f(z)=\sqrt{z}$, which corresponds to the Euclidean distance.

The weights allow us to emphasis features differently. If features are ordinal or categorical, then we should define the distance suitably. A natural choice is $d_i(x_i,x_i')=1$ if $x_i=x_i'$, otherwise, $0$.

**<u>Multi-Dimensional Scaling (MDS)</u>**

It may be easier to define similarity between objects than embed them in Euclidean space. However, some algorithms such as $K$-means require points to be in Euclidean space, so we need to tranform similarity measures into Euclidean space. MDS gives a way to find an embedding of the data in Euclidean space that approximately respects the original distance/similarity.