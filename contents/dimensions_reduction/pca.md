---
title: Principal Component Analysis 
---

# Principal Component Analysis (PCA)

**<u>Dimensionality Reduction</u>**

Real-world data can have redundant information and may have correlated variables. Dimensionality reduction project the data into a lower dimensional subspace while still capturing the essence of the original data. It is usually considered as a preprocessing step before applying other learning algorithms.

PCA is used for dimensionality reduction by identifying a small number of directions which explan most variation in data.

## PCA

First we find the principal component as the line passing through the multidimensional mean and minimises the sum of squares of the error.

The second principal component is computed in the same way as the first principal component, after correlation with first principal component has been subtracted from the points. The second principal component is **orthogonal** to the first principal component (PC).

**<u>Two Views of PCA</u>**

<u>**Maximum Variance**</u>: we want to find $k$ directions that maximise the variance in the data.

* The first PC $v_1$ is the direction of largest variance.
* The second PC $v_2$ is the direction of largest variance orthogonal to $v_1$.
* The $i$th PC $v_i$ is the direction of largest variance orthogonal to $v_1,\cdots,v_{i-1}$.

Formally, our data matrix is $X=[x_1^T,\cdots,x_N^T]^T$, and our goal is find $v_1\in\mathbb{R}^D$, $||v_1||=1$ that maximizes $||Xv_1||^2$.

The solution can be derived by:

* Let $z=Xv_1=[x_1^Tv_1,\cdots,x_N^Tv_1]$, so $z_i=x_i^Tv_1$.

* $||Xv_1||^2$ is the variance of the projections of data onto $v_1$, then we have
  $$
  ||Xv_1||^2=\sum_{i=1}^Nz_i^2=\sum_{i=1}^Nz^Tz=v_1^TX^TXv_1
  $$

* This assumes that the data is centred $\sum_ix_i=0$.

* Find $v_2,v_3,\cdots,v_k$ that are all successively orthogonal to previous directions and maximise variance.
  $$
  X_j=X-\sum_{s=1}^{j-1}Xv_sv_s^T
  $$

By applying the Rayleigh quotient, we can get
$$
v_1=\text{argmax}(v_1^TX^TXv_1)=\text{argmax}(\frac{v_1^TX^TXv_1}{v_1^Tv_1})
$$
Since $X^TX$ is PSD, the largest eigenvalue of $X^TX$ is the maximum value attained by $v_1^TX^TXv_1$ for $||v_1||=1$. At this moment, $v_1$ is the corresponding eigenvector.

<u>**Best Reconstruction**</u>: we want to find $k$ dimensional subspace with least reconstruction error.

Formally, given i.i.d data matrix $X=[x_1^T,\cdots,x_N^T]^T$, our goal is to find a $k$-dimensional linear projection that best represents the data.

In case of one PC, the solution is:

* Let $v_1$ be the direction of projection
* The point $x_i$ is mapped to $\tilde{x_i}=(v_1x_i)v_1$ where $||v_1||=1$
* We minimise the reconstruction error $\sum_{i=1}^N||x_i-\tilde{x_i}||^2$.

In case of $k$ PCs, the solution is 

* Suppose $V_k\in\mathbb{R}^{D\times k}$ is such that columns of $V_k$ are orthogonal.
* Project data $X$ onto subspace defined by $V:Z=XV_k$.
* Minimise the reconstruction error $\sum_{i=1}^N||x_i-V_kV_k^Tx_i||^2$.

**<u>Finding PCs with SVD</u>**

PCA can be associated with SVD of $X$, the first $k$ PCs are the first $k$ columns of $V$.

* PCA is the linear transformation $Z=XV$.
* Let $V_k$ be the first $k$ columns in $V$, then the dimensionality reduction via PCA: $Z_k=XV_k$.

