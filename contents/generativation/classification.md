---
title: Generative Models for Classification
---

**<u>Discriminative vs. Generative Models</u>**

Discriminative settings model the conditional distribution of the output $y$ given the input $x$ and the model parameters $\theta$.
$$
P(y|x,\theta)
$$
The generative setting model the full joint distribution of input $x$ and output $y$ given the model parameters $\theta$
$$
P(x,y|\theta)
$$

### Generative Models for Classification

Given generative model $p(x,y|\theta)$ representing the joint distribution of $x$ and $y$, then for a new input $x_\text{new}$, the conditional distribution for $y$ is 
$$
P(y=c|x_\text{new},\theta)=\frac{p(y=c|\theta)p(x_{new}|y=c,\theta)}{\sum_{c'=1}^Cp(y=c'|\theta)p(x_{new}|y=c',\theta)}
$$
where $c\in\{1,\cdots,C\}$ are the possible categories/classes for $y$.

* The numerator is the joint probability distribution $p(x_{new},y=c|\theta)$.
* The denominator is the marginal distribution $p(x_{new}|\theta)$.

To predict the most likely class, $\hat{y}=\text{argmin}_{c}P(y=c|x_\text{new},\theta)$.

In order to fit a generative model, we express the joint distribution as
$$
P(x,y|\theta,\pi)=P(y|\pi)P(x|y,\theta)
$$

* $P(y|\pi)$ is the marginal distribution of outputs $y$ parameterised by $\pi$.

  We use the parameters $\pi_c$ such that $\sum_c\pi_c=1$ and model the distribution as
  $$
  P(y=c|\pi)=\pi_c
  $$

* 

* $P(x|y,\theta)$ is the class-conditional distributions of input $x$ given the class label $y$, parameterised by $\theta$. 

