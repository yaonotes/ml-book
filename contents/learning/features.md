---
title: Feature Selection
---

# Feature Selection

In the feature selection phase, our goal is select features that are relevant to the model to learn, given a set of possible features.

**<u>Premise in feature selection</u>**

* Data contains redundant or irrelevant features.
* Removing them does not incur loss of information
* Relevant features maybe redundant in the presence of another relevant feature. Hence two features maybe strongly correlated.

**<u>Reasons for Feature Selection</u>**

* Enahnced generalisation by reducing overfitting.
* Avoid the curse of dimensionality.
* Shorter training times.
* Simplified models that are easier to interpret by users.

**<u>Feature Selection Methods</u>**

* We need search technique for subsets of a given set of features.
* We need evaluation measurement to score the different feature subsets.
  * **Wrapper methods**: train a new model for each featuresubset. Score is the count of the number of mistakes on the hold-out set. This method is expensive, yet usually provides the best performing feature set.
  * **Filter methods**: use proxy measurement as score instead of the actual test error. It exposes relationships between features. Typical scores include mutual information, Pearson correlation coefficient. It is fast to compute, yet resulting feature set not tuned to a specific type of models.
  * **Embeded methods**: Perform feature selection whiel constructing the model (e.g. regularizer). Typical examples include LASSO (non-zero parameters for corresponding features) and elastic net regularisation (combines $\ell_1$ and $\ell_2$ regularisations).

**<u>Forward Stepwise Selection</u>**

For $n$ features, we ask the following sequence of $n$ questions:

* What is the best $1$-feature model? Let the chosen feature be $f_1$.
* What is the best $i$-feature model that also ahs the previously selected features $f_1,\cdots,f_{i-1}$? Let the newly chosen feature be $f_i$.

The output is the best seen $k$-feature model for any $1\leq k\leq n$.

**Analysis**: At each step $i$, it trains and tests $n-i+1$ new models, and hence $O(n^2)$ models to train and test in total. For linear regression, it is the same complexity as building one model.

**<u>Feature Selection via Mutual Information</u>**

The mutual information for two random variables $X$ and $Y$ is defined as
$$
I(X,Y)=\sum_x\sum_yP(X=x,Y=y)\text{log}\frac{P(X=x,Y=y)}{P(X=x)P(Y=y)}
$$
**<u>Approach</u>**

* Compute mutual information for each feature and the label/target.
* Only keep the features that provide information about output
  * Ranking of features instead of finding the best subset.
  * Cut-off point using cross-validation.

**<u>Computational Considerations</u>**

The probabilities $P(X=x), P(Y=y)$ and $P(X=x,Y=y)$ can be empirically obtained from the training set.

* $P(X=x)$ is the fraction of the number of samples with $X=x$ over the number of all samples.
* Variables with continuous domains: first discretise their domains.

## Covariance vs. Correlation

**Covariance** for random variables $X$ and $Y$ measures how the random variables change jointly. It is defined as
$$
\text{cov}(X,Y)=\mathbb{E}[(X-\mathbb{E}[X])(Y-\mathbb{E}[Y])]
$$
 The (pearson) correlation coefficient normalises the covariance to give a value between $-1$ and $+1$. It is defined as
$$
\text{corr}(X,Y)=\frac{\text{cov}(X,Y)}{\sqrt{\text{cov(X,X)}\cdot\text{cov(Y,Y)}}}
$$
*Note*: Independent variables are uncorrelated, but the converse is not always true.

