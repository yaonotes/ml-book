---
title: Logistic Regression
---

# Logistic Regression

**<u>Discriminative and Classification method</u>** 

Discriminative models the conditional distribution over the output $y$ given the input $x$ and the parameters $w$.
$$
P(y|w,x)
$$
In classification, the output $y$ is categorical.

## Logistic Regression

Logistic Regression builds up on a linear model composed with sigmoid function, formally, we have
$$
P(y|w,x)=\text{Bernoulli}(\text{sigmoid}(w\cdot x))
$$
in which $w\text{log}x_0=1$, so we do not need to handle the bias term $w_0$ separately. The sigmoid function $\sigma$ is defined as
$$
\sigma(t)=\frac{1}{1+e^{-t}}, \sigma:\mathbb{R}\to(0,1)
$$
in which $t\geq 0$ and hence $\sigma(t)\geq\frac{1}{2}$.

Suppose we have estimated the model parameters $w\in\mathbb{R}^d$. For a new data points $x_{new}$, the model gives us the probability
$$
P(y_{new}=1|x_{new},w)=\sigma(w\cdot x_{new})=\frac{1}{1+\text{exp}(-x_{new}\cdot w)}
$$
In order to make a prediction, we can simply use a threshold at $\frac{1}{2}$.

* The contour line for $P(y=1|x,w)=P_0$ is given by $xw=\text{log}\frac{P_0}{1-P_0}$.
* The decision boundary is given by $P(y=1|x,w)=P(y=0|x,w)=\frac{1}{2}$, hence $xw=0$.

## Multi-Class Logistic Regression

Consider now there are $C>2$ classes and $y\in\{1,\cdots,C\}$. Then

* There are parameters $w_c\in\mathbb{R}^D$ for every class $c$.

* The parameters form a matrix $W=[w_1,\cdots,w_c]\in\mathbb{R}^{D\times C}$.

* The multi-class logistic model is given by
  $$
  P(y=c|x,W)=\frac{\text{exp}(w_c^Tx)}{\sum_{c'=1}^C\text{exp}(w_{c'}^T)x}
  $$

* Parameter estimation: NLL convex, convex optimisation.

* It can be alternatively expressed using softmax:
  $$
  P(y|x,W)=\text{softmax}([w_1^Tx,\cdots,w_C^Tx]^T)
  $$

* Two-class logistic regression is a special case where $W=[w_0,w_1]$:
  $$
  \text{softmax}([w_1^Tx,w_0^Tx]^T)=\frac{\text{exp}(w_1^Tx)}{\text{exp}(w_1^Tx)+\text{exp}(w_2^Tx)}=\sigma((w_1-w_0)^Tx)
  $$
  

## Summary

* Logistic Regression is a binary discriminative classification model.
* It can be extended to multiclass by replacing sigmoid with softmax.
* Can derive maximum likelihood estimates using convex optimization.
* Basis expansion and regularisation can be applied to logistic regression as well.
* Regularisation may be necessary if data is linearly separable.
* If the classification boundaries are non-linear
  * Polynomial or kernel-based basis expansion.
  * $\ell_1$ or $\ell_2$ regularisation if risk of overfitting.

