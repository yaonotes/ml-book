---
title: Naive Bayes
---

# Naive Bayes

Abstractly, naive Bayes is a conditional model: given a problem instance to be classified, represented by a vector $x=(x_1,\cdots,x_n)$ representing some *n* features (independent variables), it assigns to this instance probabilities.
$$
P(C_k|x_1,\cdots,x_n)
$$
for each of $K$ possible outcomes or classes $C_k$.

The problem with the above formulation is that if the number of features *n* is large or if a feature can take on a large number of values, then basing such a model on probability tables is infeasible. We therefore reformulate the model to make it more tractable. Using Bayes' theorem, the conditional probability can be decomposed as
$$
P(C_k|x)=\frac{P(C_k)P(x|C_k)}{P(x)}
$$
Note:

* $P(x)$ should be computed by total probability theorem.
* We assume that $x_1,\cdots,x_n$ are conditionally independent.

