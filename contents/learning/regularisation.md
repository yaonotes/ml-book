---
title: Regularisation
---

# Regularisation and Validation

## Ridge Regression

Suppose we have data $X\in\mathbb{R}^{N\times D}$, where $D\gg N$. One idea to avoid overfitting is to add a penalty term for weights. With a penalty term, our **least squares estimate objective** defined as
$$
\mathcal{L}(w)=(Xw-y)^T(Xw-y)
$$
changed to the **Ridge Regression Objective** defined as
$$
\mathcal{L}_\text{ridge}(w)=(Xw-y)^T(Xw-y)+\lambda\sum_{i=1}^Dw_i^2
$$
By doing so, we add a penalty term for all weights but $w_0$ to control model complexity. The effect of varying $w_0$ is output translation and not on model complexity, hence we do not control $w_0$.

In ridge regression, in addition to the residual sum of squares, we penalise the sum of squares of weights. This is also called $\ell_2$-regularization or weight-decay. Penalizing weights encourages fitting signals rather than just noise.

**<u>Weight from Ridge Regression</u>**

We first standardise the input data to make the original data becomes values drawn from standard normal distribution i.e. $\mathcal{N}(0,1)$. If we center the outputs, i.e. their mean is $0$, then $w_0$ becomes $0$ as well. Now we only need to find $w$ that minimise the ridge regression objective, i.e. 
$$
\mathcal{L}_\text{ridge}(w)=(Xw-y)^T(Xw-y)+\lambda w^Tw
$$
The gradient of the objective with respect to $w$ is
$$
\nabla_w\mathcal{L}_\text{ridge}=2(X^TX)w-2X^Ty+2\lambda w\\
=2((X^TX+\lambda I_D)w-X^Ty)
$$
If $y$ is not centred, the gradient of the objective with respect to the leading weight, or $b$, is 
$$
\nabla_b\mathcal{L}_{\text{ridge}}=2Nb-2\sum_{i=1}^Ny
$$
Set the gradient to $0$ and solve for $w$, we get
$$
(X^TX+\lambda I_D)w=X^Ty
$$
hence,
$$
w_\text{ridge}=(X^TX+\lambda I_D)^{-1}X^Ty
$$
$$
b_\text{ridge}=\frac{1}{N}\sum y_i
$$

An alternative formulation of ridge regression is by considering it as a constriained optimisation problem:
$$
\text{Minimise} (Xw-y)^T(Xw-y)\:\:\text{subject to}\:\: w^Tw\leq R
$$
The former construction is the Lagrangian formulation of this approach.

**<u>Relationship between $\lambda$ and $R$</u>**

With $\lambda$ decreasing, the magnitudes of weights start increasing. The larger $\lambda$, the smaller $R$.

## LASSO Regression

LASSO represents **L**east **A**bsolute **S**hrinkage and **S**election **O**perator. The objective function of LASSO regression is defined as
$$
\mathcal{L}_\text{Lasso}(w)=(Xw-y)^T(Xw-y)+\lambda\sum_{i=1}^D|w_i|
$$

* As with ridge regression, there is a penalty on the weights.

* The absolute value function does not allow for a simple closed-form expression. It is also called $\ell_1$-regularization.

* Similar to Ridge regression, there is an alternative formulation as below:
  $$
  \text{Minimise}\: (Xw-y)^T(Xw-y)\:\:\text{subject to}\:\: \sum_{i=1}^D|w_i|\leq R
  $$

* As $\lambda$ decreasing, the $R$ is increasing and the magnitudes of weights start increasing.

## Hyper-Parameter Choice

For ridge regression or lasso or other regularizers, we need to choose $\lambda$. To do so, we divide the data into training and validation set. Then we pick the value of $\lambda$ that minimises the validation error.

If we perform basis expansion,

* For kernels, we need to pick the width parameter $\gamma$.
* For polynomials, we need to pick degree $d$.

Grid search is the main and trivial approach for selecting hyper-parameters. There are generally four steps:

* Assume a small domain $D_i$ for each hyper-parameter $\lambda_i$.
* Iterate over all possible combinations of hyper-parameter values $D_1\times\cdots\times D_k$.
* Perform cross-validation for each combination.
* Pick the combination with the lowest validation error.

**<u>$K$-Fold Cross Validation</u>**

When the data is scarce, we can divide the data into $K$ folds (parts), instead of splitting as training and validation. Then we use $K-1$ folds for training and $1$ folds for validation.

* We commonly set $K=5$ or $K=10$.
* When $K$ is the number of datapoints, it is called LOOCV (Leave one out cross-validation).
* After the cross-validation, the validation error for fixed hyper-parameter values is the average over all runs.

