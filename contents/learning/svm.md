---
title: Support Vector MAchines
---

# Support Vector Machines

SVM is a popular discriminative model for classification.

## Maximum Margin Principle

**<u>Background</u>**

Data is linearly separable if there exists a linear separator that classifies all points correctly.

For a data point, its distance to the separating hyperplane is called the margin. When picking the separating boundary, we wish to pick one that **maximises the smallest marging** (the least distance between data and the boundary). That is, we want to maximise the distance of the closest point from the decision boundary. The points that are closet to the decision boundary are called **support vectors**.

Given a hyperplane $H:=w\cdot a+w_0=0$ and a point $x\in\mathbb{R}^d$, the distance between $x$ and $H$ can be computed by 
$$
d=\frac{w\cdot x+w_0}{||w||_2}
$$

* Positive halfspace: $wx+w_0>0$
* Negative halfspace: $wx+w_0<0$

**<u>Formulation as Optimisation Problem</u>**

Our goal as stated before can be formulated as

Find distance $||x-x^\star||_2$ between point $x^\star$ and the hyperplane $w\cdot x+w_0=0$ where the distance is given by the point $x$ on the hyperplane that is closest to $x^\star$.

Equivalently, we can formulate it as an optimisation problem. Formally, we want to find $x$ that optimises the following problem
$$
\text{minimise}\:\: ||x-x^\star||_2^2 \:\: \text{subject to}\:\: w\cdot x+w_0=0
$$
The lagrangian can be formulated as
$$
\wedge(x,\lambda)=||x-x^\star||_2^2 -2\lambda(w\cdot x+w_0)
$$

$$
=||x||_2^2-2(x^\star+\lambda w)\cdot x-2\lambda w_0+||x^\star||_2^2
$$

By setting the gradient of the lagrangian to $0$, we can find its critial point, i.e.
$$
\nabla_x\wedge(x,\lambda)=2x-2x^\star-2\lambda w=0
$$
Then we have
$$
x=x^\star+\lambda w
$$
Then by substituting $x$ into the hyperplane equation, we have
$$
w(x^\star+\lambda w)+w_0=0 
$$
hence
$$
\lambda=-\frac{wx^\star+w_0}{w\cdot w}=-\frac{wx^\star+w_0}{||w||_2^2}
$$
**<u>Linearly Separable Case</u>**

When the dataset $D=(x_i,y_i)$ is linearly separable, we have
$$
y_i(wx_i+w_0)>0
$$
or
$$
y_i(\frac{w}{\epsilon}x_i+\frac{w_0}{\epsilon})\geq 1
$$
**<u>Margin Maximisation</u>**

The margin of data point $x_i$ to hyperplane is defined as
$$
\frac{|wx_i+w_0|}{||w||_2}=\frac{y_i(wx_i+w_0)}{||w||_2}\geq \frac{1}{||w||_2}
$$
We want to pick a boundary that maximises the margin
$$
\text{maximise}\:\: \frac{1}{||w||_2}\:\text{subject to}\: y_i(wx_i+w_0)\geq 1
$$
Equivalently, we can minimise the squared $\ell_2$ norm of $w$ subject to the constraints.
$$
\text{minimise}\:\: ||w||_2\:\text{subject to}\: y_i(wx_i+w_0)\geq 1
$$

* The objective is a convex quadratic function
* The constraints are convex linear functions
* The feasible set as defined by the constraints is convex as well.

Hence, our optimisation problem is convex quadratic and it solvable using generic convex optimisation methods.

**<u>Relaxation of the SVM Formulation</u>**

Sometimes, the data is not linearly-separable. In this case, we want to find a separator that makes the least mistakes on the training error. That is, we need to satisfy as many of the $N$ constraints as possible. Alternatively, we allow all constraints to be satisified with some slack variable $\xi_i$. Then the optimisation problem becomes
$$
\text{minimise} ||w||_2^2+C\sum_{i=1}^N\xi_i
$$

$$
\text{subject to}\: y_i(wx_i+w_0)\geq 1-\xi_i,\:\:\xi_i\geq 0
$$

With $\xi_i$, a feasible solution always exists because we can let $\xi_i$ very large. The larger the $\xi_i$, the constraints can be violated as much as necessary.

* Imagine $\xi_i\ll 0$ and $x_i$ are correctly classified by a huge margin.
* The constraints will compensate the penalty incurred on misclassified points.
* $\xi\geq 0$ ensures no bonus for correct classification by a huge margin.

With relaxation, the optimal solution must satisfy

* either $\xi_i=0$. No slack is needed to classify $x_i$.
* $\xi_i=1-y_i(w\cdot x_i+w_0)\geq0$: Minimal $\xi_i$ that satisfies the constraint.

**<u>Hinge Loss</u>**

The hinge loss is defined as
$$
\ell_{\text{hinge}}:=\text{max}\{0, 1-y_i(wx_i+w_0)\}
$$
Our SVM formulation becomes equivalent to minimising the objective function
$$
\ell_{\text{SVM}}(w,w_0|X,y)=C\sum_{i=1}^N\ell_{\text{hinge}}(w,w_0;x_i,y_i)+||w||_2^2
$$

## Dual SVM Formulation

The lagrange function of SVM can be formulated as
$$
\wedge(w,w_0;\alpha)=||w||_2^2-\sum_{i=1}^N\alpha_i(y_i(wx_i+w_0)-1)
$$
in which $w$ are the primal variables, $\alpha_i$ are the dual variables and $(y_i(wx_i+w_0)-1)$ is the constraints.

The gradients w.r.t the primal variables are
$$
\frac{\partial \wedge}{\partial w_0}=-\sum_{i=1}^N\alpha_iy_i
$$

$$
\nabla_w\wedge=w-\sum_{i=1}^N\alpha_iy_ix_i
$$

Set the gradients to zero, we get
$$
\sum_{i=1}^N\alpha_iy_i=0,\:\: w=\sum_{i=1}^N\alpha_iy_ix_i
$$
For $w_0$, we can calculate from the constraints, and we will get
$$
y_i^2((\sum_{j=1}^N\alpha_jy_jx_j)x_i+w_0)=y_i
$$
hence
$$
w_0=y_i-\sum_{j=1}^N\alpha_jy_j(x_jx_i)
$$


It is numerically more stable to have $w_0$ the average over all possible values
$$
w_0=\frac{1}{N}\sum_{i=1}^N(y_i-\sum_{j=1}^N\alpha_jy_j(x_jx_i))
$$
For $\alpha$, we can plug the optimal $w$ and constraint $\sum_{i=1}^N\alpha_iy_j=0$ into Lagrangian,
$$
g(\alpha)=\sum_{i=1}^N\alpha_i-\sum_{i=1}^N\sum_{j=1}^N\alpha_iy_i\alpha_jy_j(x_ix_j)
$$
To find the critical points of $\wedge$, it is sufficient to find the critical points of $g$ that satisfy the constraints
$$
\alpha_i\geq0\:\: \text{and}\:\: \sum_{i=1}^N\alpha_iy_i=0
$$
Compared with Primal form, the dual form

* is a Quadratic concave problem
* there are $N$ variables
* it is a very simple box constraints + one zero-sum constraint.

### Predictions with SVM Dual

Prediction on a new point $x_\text{new}$ requires inner products with the support vectors
$$
wx_\text{new}=\sum_{i=1}^N\alpha_iy_i(x_ix_\text{new})
$$
We can as well use blackbox access to a function $\kappa$ that maps two inputs $(x,x')$ to their inner product $(xx')$. This is called a kernel function.