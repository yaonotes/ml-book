---
title: Maximum Likelihood
---

# Maximum Likelihood

## Model and Loss Function Choice

**<u>Optimization view of Machine Learning</u>**

* Pick a model that we expect may fit the data well enough.
* Pick a measure of performance that makes sense and can be optimised.
* Run optimization algorithm to obtain model parameters.

**<u>Probabilistic View of Machine Learning</u>**

* Pick a model for data and explicitly formulate the deviation (or uncertainty) from the model using probability.
* Use notions from probability to define suitability of various models.
* Find the parameters or make predictions on unseen data using these suitability criteria.

From a probability perspective, we can approach linear regression as well.



## Maximum Likelihood

Given observations $x_1,\cdots,x_N$ indepdently and identically drawn from the same distribution $P$ where $P$ has a parameter $\theta$.

The likelihood of observing $x_1,\cdots,x_N$ is defined as the probability of making these observations assuming they are generated according to $P$. Formally, the likelihood is defined as $P(x_1,\cdots,x_N|\theta)$: the joint probability distribution of the observations given $\theta$.

**<u>Maximum Likelihood Principle</u>**

We pick the parameter $\theta$ that maximises the likelihood. Formally, we choose 
$$
\theta^\star=\text{argmax}_\theta P(x_1,\cdots,x_N|\theta)
$$
Since we know the observations are i.i.d, we have 
$$
p(x_1,\cdots,x_N|\theta)=\prod_{i=1}^NP(x_i|\theta)
$$

## MLE in Linear Regression

Recall that our linear model is $y=wx+\epsilon$. Given $x, w$, the model $y$ can be regarded as a random variable with mean $w^Tx$.

We specifically assume that given $x,w$, then $y$ is normal with mean $w^Tx$ and variance $\sigma^2$. Formally we have
$$
P(y|w,x)=\mathcal{N}(w^Tx,\sigma^2)=w^Tx+\mathcal{N}(0,\sigma^2)
$$
Alternatively, we can view this model as $\epsilon\sim\mathcal{N}(0,\sigma^2)$. This is called Gaussian noise.

This is a **discriminative framework** in which we do not model a distribution over $X$ as the input is fixed.

**<u>Discriminative/Generative framework</u>**

* Discriminative modeling studies the $P(y|X)$ or the direct maps between the given unobserved variable (target) $X$ and a class label $y$ depended on the observed variables (training samples). 
* Generative modelling, which studies from the joint probability $P(X,y)$.

### Likelihood of Linear Regression

With the observed data $(X,y)$ made up of $(x_1,y_1),\cdots,(x_N,y_N)$, we want to know the likelihood of observing the data for model parameters $w$ and $\sigma^2$. To do so, there are two estimators available.

* MLE Estimator: Find parameters which maximise the likelihood.
* Least Square Estimator: Find parameters which minimise the sum of squares of the residuals.

**<u>MLE Estimator</u>**

With the observed data $(x_1,y_1),\cdots,(x_N,y_N)$, the likelihood of observing the data for the parameters $w,\sigma$ is given by
$$
P(y|X,w,\sigma)=P(y_1,\cdots,y_N|x_1,\cdots,x_N,w,\sigma)=\prod_{i=1}^NP(y_i|x_i,w,\sigma)
$$
Since we assume the model is normal, we have $y_i\sim\mathcal{N}(w^Tx_i,\sigma^2)$, we have
$$
P(y_1,\cdots,y_N|x_1,\cdots,x_N,w,\sigma)=\prod_{i=1}^N\frac{1}{\sqrt{2\pi\sigma^2}}\text{exp}(-\frac{(y_i-w^Tx_i)^2}{2\sigma^2})\\
$$
$$
=(\frac{1}{2\pi\sigma^2})^{N/2}\text{exp}(-\frac{1}{2\sigma^2}\sum_{i=1}^N(y_i-w^Tx_i)^2)\\
$$

$$
=(\frac{1}{2\pi\sigma^2})^{N/2}\text{exp}(-\frac{1}{2\sigma^2}\sum_{i=1}^N(Xw-y)^T(Xw-y))
$$



Then the negative log-likelihood will become
$$
\text{NLL}(y|X,w,\sigma)=-\text{LL}(y|X,w,\sigma)=\frac{N}{2}\text{log}(2\pi\sigma^2)+\frac{1}{2\sigma^2}(Xw-y)^T(Xw-y)
$$
In order to maximise the likelihood, it is equivalent to minimise the negative log-likelihood. Similar to least square estimator, we end up with
$$
\hat{w}_{\text{ML}}=(X^TX)^{-1}X^Ty
$$
This result is the same as the least square estimator.

The for $\sigma^2$, we have
$$
\sigma^2_{\text{ML}}=\frac{1}{N}(Xw_{\text{ML}}-y)^T(Xw_\text{ML}-y)
$$
To make predictions, we have
$$
\hat{y} = w_{\text{ML}}x
$$
and we have the confidence interval as
$$
y\sim \hat{y}+\mathcal{N}(0,\sigma^2)
$$
