---
title: Multi-Layer Perceptrons
---

# Perceptron Algorithms

The input to the perceptron algorithm is the feature vector $x$ and the label $y$. The output is $\text{sign}(b+wx)$ where $b$ is the bias term and $w$ is the weight parameters.



