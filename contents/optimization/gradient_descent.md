---
title: Gradient Based Optimisation
---

# Gradient Based Optimisation

**<u>Theorem of Gradient</u>**

If a function $f$ is differentiable, the gradient of $f$ at that point is either zero or perpendicular to the contour line of $f$ at that point.

## Background

**<u>Gradient Points in the Direction of Steepest Increase</u>**

Each component of the gradient says how fast the function changes w.r.t the standard basis
$$
\frac{\partial f}{\partial x}(a)=\lim_{h\to0}\frac{f(a+h\textbf{i})-f(a)}{h}
$$
where $\textbf{i}$ is the unit vector in the direction of $x$, i.e. it captures information in which direction we move.

We can also change w.r.t the direction of some arbitrary vector $\textbf{v}$. The derivative in direction of $\textbf{v}$ is called **directional derivative**.
$$
\nabla_vf(a)=\lim_{h\to0}\frac{f(a+h\textbf{v})-f(a)}{h}=\nabla f(a)\textbf{v}
$$
From an geometric perspective, we multiply $||\nabla f(a)||$ by the scalar projection of $\textbf{v}$. Hence $\nabla f(a)\textbf{v}=||\nabla f(a)||\cdot||v||\cos\theta$ where $\cos\theta$ is the angle between $\nabla_f (a)$ and $\textbf{v}$. The maximal value is for $\cos\theta=1$, or $\theta=0$, so $\nabla f(a)$ and $\textbf{v}$ have the same direction.

**<u>Hessian Matrix</u>**

Hessian matrix $H$ is the matrix of all second-order partial derivatives of $f$.

* It is symmetric as long as all second derivatives exist.
* It captures the curvature of the surface.

There are several cases regarding the eigenvalues of $H$:

* $H$ has positive eigenvalues, then the point $x$ is local minimum.
* $H$ has negative eigenvalues, then the point $x$ is local maximum.
* $H$ has mixed eigenvalues, then the pooint $x$ is saddle point.
* Degenerate case: If the eigenvalue is $0$, then there is no inverse and the gradient is unchanging.

## Gradient Descent

Gradient descent is one of the simplest, but very general optimisation algorithm for finding a local minimum of a differentiable function. It is iterative and produces a new vector $w_{t+1}$ at each iteration $t$:
$$
w_{t+1}=w_t-\eta_tg_t=w_t-\eta_t\nabla f(w_t)
$$
where $\eta_t>0$ is the learning rate or step size.

At each iteration, it moves in the direction of the steepest descent.

### Gradient Descent for Least Squares Regression

Recall the objective function for least squares regression
$$
\mathcal{L}(w)=(Xw-y)^T(Xw-y)=\sum_{i=1}^N(x_i^Tw-y_i)^2
$$
We can compute the gradient of $\mathcal{L}$ with respect to $w$ as
$$
\nabla_w\mathcal{L}=2(X^TXw-X^Ty)
$$
The complexity for gradient descent vs. closed-form solution for very large and wide datasets:

* Computational Complexity of inverting matrices in closed-form solution.
* Each gradient calculation is linear in $N$ and $D$.

**<u>Learning Rate (Step Size) Choice</u>**

Choosing a good learning rate is key and we may want a time-varying step size

* If the step size is too large, algorithms may nerver converge.
* If the step size is too small, convergence may be very slow.

When choosing the learning rate, we have the following options:

* Constant step size: $\eta_t=c$.
* Decaying step size: $\eta_t=c/t$. Different rates of decay common, e.g. $\frac{1}{\sqrt{t}}$.
* Backtracking line search.
  * Start with $c/t$, usually a large value.
  * Check for a decrease: Is $f(w_t-\eta_t\nabla f(w))<f(w_t)$?
  * If decrease condition not met, multiply $\eta_t$ by a decaying factor, e.g. $0.5$.
  * Repeat until the decrease condition is met.

**<u>Test for Convergence</u>**

* Fixed number of iterations: Terminate if $t\geq T$.
* Small increase: Terminate if $f(w_{t+1})-f(w_t)\leq\epsilon$.
* Small change: Terminate if $||w_{t+1}-w_t||\leq \epsilon$.

### Stochastic Gradient Descent (SGD)

We minimise the objective function over data points $(x_1,y_1),\cdots,(x_N,y_N)$, and our objective function is defined as
$$
\mathcal{L}(w)=\frac{1}{N}\sum_{i=1}^N\ell(w;x_i,y_i)+\lambda\mathcal{R}(w)
$$
Where the $\lambda\mathcal{R}(w)$ is the regularisation term.

The gradient of the objective function is
$$
\nabla_w\mathcal{L}=\frac{1}{N}\sum_{i=1}^N\nabla_w\ell(w;x_i,y_i)+\lambda\nabla_w\mathcal{R}(w)
$$
For example, with Ridge Regression, we have
$$
\mathcal{L}(w)=\frac{1}{N}\sum_{i=1}^N(w^Tx_i-y_i)^2+\lambda w^Tw
$$

$$
\nabla_w\mathcal{L}=\frac{1}{N}\sum_{i=1}^N2(w^Tx_i-y_i)x_i+2\lambda w
$$

As part of the learning algorithm, we calculate the gradient. Suppose we pick a random datapoint $(x_i,y_i)$ and evaluate $g_i=\nabla_w\ell(w;x_i,y_i)$. Then we have
$$
\mathbb{E}[g_i]=\frac{1}{N}\sum_{i=1}^N\nabla_w\ell(w;x_i,y_i)
$$
In the expectation of $g_i$, it points in the same direction as the entire gradient (except for the regularisation term).

In SGD, we compute the gradient at one data point instead of at all data points.

In practice, **mini-batch gradient descent** significantly improves the performance, and reduces the variance in the gradients and hence, it is more stable than SGD.

### Sub-Gradient Descent

**<u>Mimising the Lasso Objective</u>**

Recall that in the Lasso objective, i.e. linear model with $\ell_1$-regularisation, we have
$$
\mathcal{L}(w)=\sum_{i=1}^N(w^Tx_i-y_i)^2+\lambda\sum_{i=1}^D|w_i|
$$

$$
=w^Tx^Txw-2y^TXw+y^Ty+\lambda\sum_{i=1}^D|w_i|
$$

* Quadratic part of the loss function can't be framed as linear programming.
* Lasso regularisation does not allow for closed-form solutions.
* It must be resorted to general optimisation methods.
* The objective function is not differentiable.

The sub-gradient descent focuses on the case when $f$ is convex:
$$
f(\alpha x+(1-\alpha)y)\leq\alpha f(x)+(1-\alpha)f(y), \forall x,y, \forall \alpha\in[0,1]
$$
At $x_0$, any $g$ satisfying the following inequality is called a sub-gradient at $x_0$:

* $f(x)\geq f(x_0)+g(x-x_0)$, where $g$ is a sub-derivative.
* $f(x)\geq f(x_0)+g^T(x-x_0)$, where $g$ is a sub-gradient.

## Constrained Convex Optimisation

With constraint on the parameters, we extend the approach from gradient  descnt to projected gradient descent.

### Projected Gradient Descent

The goal of Projected Gradient Descent is to minimises $f(x)$ subject to additional constraints $w_C\in C$. Formally, we have
$$
z_{t+1}=w_t-\eta_t\nabla_f(w_t)
$$

$$
w_{t+1}=\overset{}{\text{argmin}}_{w_c\in C}||z_{t+1}-w_c||
$$

Gradient step is followed by a **projection step**.

## Other methods

### Second Order Methods

In calculus, our goal is find the roots of a differentiable function $f$, i.e. solutions to $f(x)=0$. In optimisations, the goal is to find roots of $f'$, i.e. solutions to $f'(x)=0$. In this case,

* Function $f$ needs to be twice-differentiable.
* The roots of $f'$ are stationary points of $f$, i.e. minima/maxima/saddle points.

**<u>Newton Methods in One Dimension</u>**

* Construct a sequence of points $x_1,\cdots,x_n$ starting with an initial guess $x_0$.

* Sequence converges towards a minimiser $x^\star$ of $f$ using the sequence of **second-order Taylor approximations of $f$ around the iterates**:
  $$
  f(x)\approx f(x_k)+(x-x_k)f'(x_k)+\frac{1}{2}(x-x_k)^2 f''(x_k)
  $$

* $x_{k+1}=x^\star$ defined as the minimiser of this quadratic approximation.

* If $f''$ is positive, then the quadratic approximation is **convex**, and a minimiser is obtained by setting the derivative to zero:
  $$
  0=\frac{d}{dx}(f(x_k)+(x-x_k)f'(x_k)+\frac{1}{2}(x-x_k)^2f''(x_k))=f'(x_k)+(x^\star-x_k)f''(x_k)
  $$
  Then we have
  $$
  x_{k+1}=x^\star=x_k-f'(x_k)[f''(x_k)]^{-1}
  $$
  

