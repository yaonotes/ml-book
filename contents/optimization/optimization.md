---
title: Optimization
---

# Optimization

Most machine learning methods can be cast as optimisation problems. Besides of closed-form solutions, there are generally two approaches to solving the problems beyond closed-form solutions:

* Frame the objective of the machine learning problem as a mathematical problem, and use existing blackbox solver for such problems. This is possible when objectives can be formulated as convex optimisation problems.
* Gradient-based optimisation methods. 

## Convex

**<u>Convex Sets</u>**

A set $C\subseteq\mathbb{R}^D$ is convex if for any $x,y\in C$ and $\lambda\in[0,1]$, we have 
$$
\lambda x+(1-\lambda)y\in C
$$
Some examples of convex sets include

* Set $\mathbb{R}^d$.
* Intersections of convex sets.
* Norm balls. For any norm $||\cdot||$, the set $B=\{x\in\mathbb{R}^d:||x||\leq 1\}$ is convex.
* Polyhedra. Given $A\in\mathbb{R}^{m\times n}$ and $b\in\mathbb{R}^m$, the polyhedron $\{x\in\mathbb{R}^n:Ax\leq b\}$ is convex.
* Positive semidefinite cone: The set of positive semi-definite (PSD) matrices.

**<u>Convex Functions</u>**

A function $f:\mathbb{R}^n\to\mathbb{R}^n$ defined on a convex domain is convex if
$$
\forall x,y\in\mathbb{R}^D, 0\leq\lambda\leq1
$$
we have
$$
f(\lambda x+(1-\lambda)y)\leq\lambda f(x)+(1-\lambda)f(y)
$$
Some examples of convex functions include

* Affine functions: $f(x)=b^Tx+c$.
* Quadratic functions: $f(x)=x^TAx+b^Tx+c$ where $A$ is symmetric positive semi-definite.
* Nonnegative weighted sums of convex functions.
* Norms: $||\cdot||_p$ except $p=0$.

**<u>Convex Optimisation</u>**

Given convex function $f(x),g_1(x),\cdots,g_m(x)$ and affine functions $h_1(x),\cdots,h_n(x)$. A convex optimisation problem is of the form:
$$
\text{minimize} f(x)\:\: \text{subject to}\:\:g_i(x)\leq0\:\text{and}\:h_j(x)=0
$$
where $i\in\{1,\cdots,m\}$ and $j\in\{1,\cdots,n\}$.

The goal is find an optimal value of a convex optimisation problem:
$$
V^\star=\text{min}\{f(x):g_i(x)\leq 0, i\in\{1,\cdots, m\}, h_j(x)=0, j\in\{1,\cdots,n\}\}
$$
Whenever $f(x^\star)=V^\star$, then $x^\star$ is a (not necessarily) unique optimal point. 

* $V^\star:=\infty$ for infeasible instances. (Feasible means fulfils all constraints $g_i$ and $h_j$).
* $V^\star:=-\infty$ for unbounded instances. (Unbounded means the set of feasible instances has no infimum).

**<u>Local Optima and Global Optima</u>**

$x$ is locally optimal if

* $x$ is feasible.
* There is $B>0$ such that $f(x)\leq f(y)$ for all feasble $y$ with $||x-y||_2\leq B$.

$x$ is globally optimal if

* $x$ is feasible.
* $f(x)\leq f(y)$ for all feasible $y$.

***Theorem***: For a convex optimisation problem, all locally optimal points are globally optimal.

## Convex Optimisation Problems

### Linear Programming

$$
\text{minimize}\: c^Tx+d\: \text{subject to}\: Ax\leq e\: \text{and}\: Bx=f
$$

There is no closed-form solution for this class of problems. However, efficient algorithms exist, both in theory and practice (for tens of thousands of variables).

**<u>Linear Model with Absolute Loss</u>**

Suppose we have data $(X,y)$ and that we want to minimise the objective
$$
\mathcal{L}(w)=\sum_{i=1}^N|x_i^Tw-y_i|
$$
We want to transform this optimisation problem into a linear program. We introduce a $\xi_i$ for each data point.

The linear program in the $D+N$ variables $w_1,\cdots,w_D,\xi_1,\cdots,\xi_N$. Then our linear program is 
$$
\text{minimize}\:\: \sum_{i=1}^N\xi_i
$$

$$
\text{subject to}\:\: w^Tx_i-y_i\leq \xi_i\:\:\text{and} y_i-w^Tx_i\leq \xi_i
$$

where $i\in\{1,\cdots,N\}$.

The solution to this linear program gives $w$ that minimises the objective $\mathcal{L}$.

**<u>Maximise Likelihood</u>**

Calculating the maximum likelihood is a convex quadratic optimisation problem with no constraints.

**<u>Mimising the Lasso Objective</u>**

For the Lasso objective, i.e. linear model with $\ell_1$-regularisation, we have
$$
\mathcal{L}(w)=\sum_{i=1}^N(w^Tx_i-y_i)^2+\lambda\sum_{i=1}^D|w_i|
$$

$$
=w^Tx^Txw-2y^TXw+y^Ty+\lambda\sum_{i=1}^D|w_i|
$$

* Quadratic part of the loss function cannot be framed as linear program.
* Lasso regularisation does not allow for closed-form solutions.
* It can be rephrased as quadratic programming problem.
* Alternatively, it can be resorted to general optimisation methods.

### Quadratically constrained Quadratic Programming

$$
\text{minimize}\:\frac{1}{2}x^TBx+c^Tx+d
$$

$$
\text{subject to}\:\frac{1}{2}x^TQ_ix+r_i^Tx+s_i\leq 0\: \text{and}\: Ax=b
$$

where $i\in\{1,\cdots,m\}$.

### Semidefinite Programming

$$
\text{minimize}\: tr(CX)\:\text{subject to}\: tr(A_iX)=b_i
$$

where $X$ is positive semidefinite and $i\in\{1,\cdots,m\}$.

