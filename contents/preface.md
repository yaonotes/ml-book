---
title: "Foundation of Data Science"
author: Xiaozhe Yao
date: "2020-12-10"
lang: "en"
titlepage: false,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
---

# Preface

[PDF Version](https://yaonotes.gitlab.io/ml-book/fds.pdf)
