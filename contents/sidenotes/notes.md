---
title: Side Notes
---

# Sidenotes

**<u>Ridge Regularizer</u>**
$$
\mathcal{L}_\text{ridge}(w)=(Xw-y)^T(Xw-y)+\lambda\sum_{i=1}^Dw_i^2
$$
**<u>Lasso Regularizer</u>**
$$
\mathcal{L}_\text{Lasso}(w)=(Xw-y)^T(Xw-y)+\lambda\sum_{i=1}^D|w_i|
$$
**<u>Generative Models and Discriminative Models</u>**

Generative $P(X|Y=y)$: Naive Bayes; Linear Discriminative Analysis;

Discriminative $P(Y|X=x)$: logistic regression; linear regression; K-NN; SVM; Neural Networks;

**<u>Newton Methods</u>**
$$
x_{k+1}=x_k-\frac{f'(x_k)}{f''(x_k)}
$$
**<u>Naive Bayes</u>**
$$
P(Y=y|X_i=x_i)=\frac{P(x_i|y=c)P(y=c)}{\sum_{y_i}P(x_i|Y=y_i)P(Y=y_i)}
$$
Example: exercise 4.4.

**<u>Linear Regression</u>**
$$
\hat{y}=Xw=X(X^TX)^{-1}X^Ty
$$
If it has solution, then the matrix $X^TX$ must be full rank $D$, and the rank is bounded by $\text{min}\{D,N\}$, hence $D\leq N$.

**<u>Distance from point to hyperplane</u>**

Hyperplane: $w^Tx+b=0$, point $x_0$, then the distance is 
$$
\frac{|w^Tx+b|}{||w||}
$$


**<u>SVM in Dual Form</u>**

